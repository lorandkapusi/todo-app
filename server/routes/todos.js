var express = require("express");
var router = express.Router();
let todos = [{ text: "Todo1", completed: false, id: 15 }];

router.get("/", function (_req, res) {
  res.send(todos);
});

router.post("/", function (req, res) {
  todos.push(req.body);
  res.json(todos);
});
router.delete("/:id", function (req, res) {
  todos = todos.filter((todo) => todo.id != req.params.id);
  res.json(todos);
});

router.put("/:id", function (req, res) {
  todos = todos.map((item) => {
    if (item.id == req.params.id) {
      item.completed = !item.completed;
      return item;
    } else {
      return item;
    }
  });
  res.json(todos);
});

module.exports = router;
