import React, { useState, useEffect } from "react";
import "./App.css";
import { Form } from "./components/Form";
import { TodoList } from "./components/TodoList";
import axios from "axios";

export const App = () => {
  const [inputText, setInputText] = useState("");
  const [todos, setTodos] = useState([]);
  const [status, setStatus] = useState("all");
  const [filteredTodos, setFilteredTodos] = useState([]);

  useEffect(() => {
    axios
      .get(`http://localhost:8080/todo`)
      .then((res) => {
        setTodos([...todos, ...res.data]);
        filterHandler([...todos, ...res.data]);
      })
      .catch((err) => {
        console.log("ERROR", err);
      });
  }, [status]);
  useEffect(() => {
    filterHandler(todos);
  }, [todos]);

  console.log("Help:", ...todos);

  const filterHandler = (todoList) => {
    switch (status) {
      case "completed":
        setFilteredTodos(todoList.filter((todo) => todo.completed === true));
        break;
      case "uncompleted":
        setFilteredTodos(todoList.filter((todo) => todo.completed === false));
        break;
      default:
        setFilteredTodos(todoList);
        break;
    }
  };

  return (
    <div className="App">
      <header id="title">Todo-List</header>
      <Form
        id="form"
        inputText={inputText}
        setInputText={setInputText}
        todos={todos}
        setStatus={setStatus}
        setTodos={setTodos}
      />
      <TodoList
        className="todo-container"
        setTodos={setTodos}
        filteredTodos={filteredTodos}
        todos={todos}
        inputText={inputText}
      />
    </div>
  );
};
