import React from "react";
import axios from "axios";

export const Form = ({
  inputText,
  setInputText,
  todos,
  setTodos,
  setStatus,
}) => {
  const inputTextHandler = (e) => {
    console.log(e.target.value);
    setInputText(e.target.value);
  };
  const submitTodoHandler = (e) => {
    let newTodo = {
      text: inputText,
      completed: false,
      id: Math.random() * 100,
    };
    e.preventDefault();
    setTodos([...todos, newTodo]);
    setInputText("");

    axios
      .post("http://localhost:8080/todo", newTodo)
      .then((_res) => {
        console.log("ok");
      })
      .catch((err) => {
        console.log("ERROR", err);
      });
  };
  const statusHandler = (e) => {
    setStatus(e.target.value);
  };

  return (
    <form className="form">
      <div className="inputAddButton">
        <input
          className="input"
          value={inputText}
          onChange={inputTextHandler}
          type="text"
          maxLength="35"
        />
        <button onClick={submitTodoHandler} className="addButton" type="submit">
          Add Todo
        </button>
      </div>
      <div className="select">
        <select onChange={statusHandler}>
          <option value="all">All</option>
          <option value="completed"> Completed</option>
          <option value="uncompleted">Uncompleted</option>
        </select>
      </div>
    </form>
  );
};
