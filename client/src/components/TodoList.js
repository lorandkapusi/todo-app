import React from "react";
import { Todo } from "./Todo";

export const TodoList = ({ todos, inputText, setTodos, filteredTodos }) => {
  return (
    <div>
      <ul className="todo-list">
        {filteredTodos.map((todo) => (
          <Todo
            setTodos={setTodos}
            todo={todo}
            todos={todos}
            text={todo.text}
            key={todo.id}
            inputText={inputText}
          />
        ))}
      </ul>
    </div>
  );
};
