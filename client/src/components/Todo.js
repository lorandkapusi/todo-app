import React from "react";
import axios from "axios";

export const Todo = ({ todo, todos, setTodos }) => {
  const deleteHandler = () => {
    setTodos(todos.filter((el) => el.id !== todo.id));
    axios
      .delete(`http://localhost:8080/todo/${todo.id}`)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log("ERROR", err);
      });
  };
  const completeHandler = () => {
    setTodos(
      todos.map((item) => {
        if (item.id === todo.id) {
          return {
            ...item,
            completed: !item.completed,
          };
        }
        return item;
      })
    );
    axios
      .put(`http://localhost:8080/todo/${todo.id}`)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log("ERROR", err);
      });
  };

  return (
    <div className="todo">
      <li className={`todo-item ${todo.completed ? "completed" : ""}`}>
        {todo.text}
      </li>
      <div className="buttons">
        <button onClick={completeHandler} className="complete-btn">
          ✓
        </button>
        <button onClick={deleteHandler} className="trash-btn">
          X
        </button>
      </div>
    </div>
  );
};
